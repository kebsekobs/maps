import pygame
import requests
import sys
import os

response = None
zoom = 10
def search(zoom):
    try:
        toponym_to_find = '53.204843 56.852593'

        geocoder_api_server = "http://geocode-maps.yandex.ru/1.x/"

        geocoder_params = {"geocode": toponym_to_find, "format": "json"}

        response = requests.get(geocoder_api_server, params=geocoder_params)
        if not response:
        # обработка ошибочной ситуации
            pass

    # Преобразуем ответ в json-объект
        json_response = response.json()
    # Получаем первый топоним из ответа геокодера.
        toponym = json_response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]
    # Координаты центра топонима:
        toponym_coodrinates = toponym["Point"]["pos"]
    # Долгота и широта:
        toponym_longitude, toponym_lattitude = toponym_coodrinates.split(" ")

    # Собираем параметры для запроса к StaticMapsAPI:
        map_params = {
            "ll": ",".join([toponym_longitude, toponym_lattitude]),
            "l": "map",
            "z": str(zoom)
        }
        map_api_server = "http://static-maps.yandex.ru/1.x/"
    # ... и выполняем запрос
        response = requests.get(map_api_server, params=map_params)
        if not response:
            print("Ошибка выполнения запроса:")
            print("Http статус:", response.status_code, "(", response.reason, ")")
            sys.exit(1)
        return response


    except:
        print("Запрос не удалось выполнить. Проверьте наличие сети Интернет.")
        sys.exit(1)


# Запишем полученное изображение в файл.
map_file = "map.png"
try:
    with open(map_file, "wb") as file:
        file.write(search(zoom).content)
except IOError as ex:
    print("Ошибка записи временного файла:", ex)
    sys.exit(2)

# Инициализируем pygame
pygame.init()
screen = pygame.display.set_mode((600, 450))
# Рисуем картинку, загружаемую из только что созданного файла.
screen.blit(pygame.image.load(map_file), (0, 0))
# Переключаем экран и ждем закрытия окна.
pygame.display.flip()
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_PAGEUP:
                if int(zoom) < 18:
                    zoom += 1
            elif event.key == pygame.K_PAGEDOWN:
                if int(zoom) > 0:
                    zoom -= 1
            map_file = "map.png"
            try:
                with open(map_file, "wb") as file:
                    file.write(search(zoom).content)
            except IOError as ex:
                print("Ошибка записи временного файла:", ex)
                sys.exit(2)
            pygame.init()
            screen = pygame.display.set_mode((600, 450))
            # Рисуем картинку, загружаемую из только что созданного файла.
            screen.blit(pygame.image.load(map_file), (0, 0))
            # Переключаем экран и ждем закрытия окна.
            pygame.display.flip()
pygame.quit()

# Удаляем за собой файл с изображением.
os.remove(map_file)